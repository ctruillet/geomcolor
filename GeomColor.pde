/*
* Author : Clement Truillet
* Versions
*   V1.0 : 19/03/2019 - First Release
*   V1.1 : 20/03/2019 - ULTRA DLC ++
*
* 4 areas
*   - RGB : Set a random color to the square if you click on.
*        Set the white color if you click on the left side of the area (same for black color on the right side)
*        Add (to the current color) some red, blue or green if you click on the red, green, blue square.
*   - Central Symmetry : Draw the point on the mouse and the symmetric point in relation to the center of the area
*   - Axial Symmetry : Draw the point on the mouse and the symmetric point in relation to the vertical line of the area
*   - Translation : The square moves toward the mouse
*
*   To reset one area, click on the area. 
*/


int x = 0;
int y = 0;
int witdhSquare = 17;
int r = (int)random(256);
int g = (int)random(256);
int b = (int)random(256);
String hex = String.format("#%02X%02X%02X", r, g, b);  ;

void setup(){
  size(800,800);
  background(218);
  
  line(width/2,0,width/2,height); //vertical line
  line(0,height/2,height,height/2); //Horizontal line
  textAlign(CENTER);
  textSize(20);
  noSmooth();
  x = width/8;
  y = 3*height/4;
  
}

void draw(){


  fill(0);
  
  //RGB Section
    text("RGB",width/4,25);
    //Red Square
    fill(256,0,0);
    square(width/4-3*witdhSquare,50,2*witdhSquare);
    
    //Green Square
    fill(0,256,0);
    square(width/4-witdhSquare,50,2*witdhSquare);
    
    //Blue Square
    fill(0,0,256);
    square(width/4+witdhSquare,50,2*witdhSquare);
    
    //Random Square
    fill(r,g,b);
    square(width/4-80,height/6,2*80);
    fill(256-r,256-g,256-b);
    text(r + ";" + g + ";" + b,width/4,220);
    text(hex,width/4,245);
    
  //Symetric Section
  //Up Section
    fill(0);
    text("Symetrie Centrale",3*width/4,25);

    line(3*width/4-5,height/4,3*width/4+5,height/4);
    line(3*width/4,height/4-5,3*width/4,height/4+5);
    if(mouseX-1 > width/2 && mouseY-1 < height/2){
      strokeWeight(5);
      point(mouseX,mouseY);
      point(mouseX+2*(3*width/4-mouseX),mouseY+2*(height/4-mouseY));

      strokeWeight(1);
    }
  //Down Section
    fill(0);
    text("Symetrie Axiale",3*width/4,height/2+25);
    line(3*width/4,height/2+40,3*width/4,height-20);
    if(mouseX-1 > width/2 && mouseY-1 > height/2){
      strokeWeight(5);
      point(mouseX,mouseY);
      point(mouseX+2*(3*width/4-mouseX),mouseY);
      strokeWeight(1);
    }
      
  
  //Translation Section
    fill(0);
    text("Translation",width/4,height/2+25);
  
  if((mouseX<width/2-25 && mouseY>height/2+41) && (x < width/2 && x >= 0 && y >= height/2+41 && y < height)){
    if(mouseX-25-x != 0){
      x+=(mouseX-25-x)/abs(mouseX-25-x);
    }
    if(mouseY-25-y != 0){
      y+=(mouseY-25-y)/abs(mouseY-25-y);
    }
    
    fill(218);
    noStroke();
    square(0,height/2+40,width/2);
    stroke(0);
    line(x+25,y+25,mouseX,mouseY);
    
  }
  fill(r,g,b);
  square(x,y,50);
}

void mousePressed(){
  if(mouseX>width/4-3*witdhSquare && mouseX<width/4-witdhSquare && mouseY>50 && mouseY<50+2*witdhSquare){ //Add red color
    r = (r+4)%256;
    hex = String.format("#%02X%02X%02X", r, g, b);  
  }else if(mouseX>width/4-witdhSquare && mouseX<width/4+witdhSquare && mouseY>50 && mouseY<50+2*witdhSquare){ //Add green color
    g = (g+4)%256;
    hex = String.format("#%02X%02X%02X", r, g, b); 
  }else if(mouseX>width/4+witdhSquare && mouseX<width/4+3*witdhSquare && mouseY>50 && mouseY<50+2*witdhSquare){ //Add blue color
    b = (b+4)%256;
    hex = String.format("#%02X%02X%02X", r, g, b); 
  }else if(mouseX>width/4-80&& mouseX<width/4+80 && mouseY>height/6 && mouseY<height/6+2*80){ //Random Square
    r = (int)random(256);
    g = (int)random(256);
    b = (int)random(256);
    hex = String.format("#%02X%02X%02X", r, g, b);  
  }else if(mouseX<width/4 && mouseY<height/2){ //Set White color
    r = 256;
    g = 256;
    b = 256;
    hex = String.format("#%02X%02X%02X", r, g, b); 
  }else if(mouseX>width/4 && mouseX<width/2 && mouseY<height/2){ //Set black color
    r = 0;
    g = 0;
    b = 0;
    hex = String.format("#%02X%02X%02X", r, g, b); 
  }else if(mouseX > width/2 && mouseY > height/2){
    fill(218);
    square(width/2,height/2,width/2);
  }else if(mouseX>width/2 && mouseY<height/2){
    fill(218);
    square(width/2,-1,width/2+1);
  }else{
    x = width/8;
    y = 3*height/4;
  }  
}
